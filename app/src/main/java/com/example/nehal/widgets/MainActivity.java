package com.example.nehal.widgets;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.ToggleButton;

import butterknife.OnItemSelected;

public class MainActivity extends DrawerActivity {
    ToggleButton toggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //this is for the toggle button
         toggle = (ToggleButton) findViewById(R.id.toggleButton);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {//if on
                    //awel argument fl snackbar.make lazem yeb2a view, hena 3shan ana ba3mel extend lel
                    // drawer activity view fa ana bast5demo ka view leh bs ma3rfsh lw msh b3mel extend to be honest
                    Snackbar.make(findViewById(R.id.drawer_layout), "Toggle is on!", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else {//if off
                    Snackbar.make(findViewById(R.id.drawer_layout), "Toggle is OFF!", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });
       //next is using spinners
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        /*
        * for spinners you can just use it with a button to chech its results, just set on click listener
        * for the button then define to get content of spinner define whatever variable i.e:
        * String planet = spinner.getSelectedItem.toString()
        *
        * */

        
//for horizontal progress bar//
        ProgressBar progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);
        progressBar2.setVisibility(ProgressBar.VISIBLE);
        progressBar2.setProgress(6);
        progressBar2.setMax(10);

    }
//handles radio button event click listeners
    // you must add in Layout the OnClick for both radio buttons
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioButton:
                if (checked){
                    Snackbar.make(findViewById(R.id.drawer_layout), "Option 1", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                    break;
            case R.id.radioButton2:
                if (checked){
                    Snackbar.make(findViewById(R.id.drawer_layout), "Option 2", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                    break;
        }
    }
}
